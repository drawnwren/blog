---
title: Hello World
date: "2018-05-23T18:38:00.000Z"
---

## Tasks Accomplished ##
- [x] Setup blog, gatsby/netlify/gitlab integration
- [x] Register domain

The first day of Summer work was fairly slow. I got this blog up and running, familiarized myself with [Gatsby JS](http://gatsbyjs.org), [ Netlify ](https://netlify.com), and integrated them with [Gitlab](http://gitlab.com)

Now that I have a place to log my daily activities, I plan to keep a record of my process of exploring business opportunities.

Roughly, I intend to iterate through approaches people are trying on forums to attempt to make a profit over the summer.

## Forums ##
- stm
- bhw

There seem to be two approaches to making money on the internet. The first is finding a spigot, usually ad revenue, and finding a way to extract money from it. The second, is to acquire users and sell them to VCs. User acquisition is the more moral approach, but finding something that users want is more complicated.

One approach to solving this problem is to start implementing websites that already exist and interest you.

## Tasks for Tomorrow ##
- [ ] Find 3 web sites that are relatively simple
- [ ] Identify 1(!) key feature from each site that I would like to implement
- [ ] Identify 2 more research avenues for small SaaS products
  - Old HN posts?
  - LinkedIn?
  - Old YC groups?
