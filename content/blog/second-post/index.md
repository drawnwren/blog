---
title: Day Two
date: "2018-05-24T18:38:00.000Z"
---
## Tasks for Today ##
- [ ] Find 3 web sites that are relatively simple
- [ ] Identify 1(!) key feature from each site that I would like to implement
- [ ] Identify 2 more research avenues for small SaaS products
  - Old HN posts?
  - LinkedIn?
  - Old YC groups?

### Startups organized by broad category
1. Startups that involve simplifying a complex task to a simpler one
  - https://forestry.io/, https://cosmicjs.com/# (simplify static sites, CMS)
  - https://simplecitizen.com/ (simplify citizenship paperwork)
  - https://upsolve.org/ (simplify bankruptcy)
  - https://www.avo.app/ (simplifies analytics)
  - https://travelchime.com/ (simplifies trip planning and collaboration with recommenders)
  - https://bookwithmatrix.com/, simplifies booking with matrix.itasoftware.com routes but seems to fall pretty short imo
  - https://rabbet.com/, simplify construction loan documentation
2. Startups that attempt to make some business task simpler, usually by generating some expected forms or analysis from user-generated data
  - https://www.finaleinventory.com/
  - https://www.distru.com/
3. Very domain specific startups (YC seems to love these)
  - https://www.jet.law/
4. Join a data stream
  - https://alltheflightdeals.com/, joins flight deals on location
5. Novel hardware
  - https://www.bottomless.com/, use a scale and an algorithm to order coffee when the user is about to run out
6. Trying to solve the hiring problem
  - https://underdog.io/, talent marketplace. One application to a large group of companies
7. Airtable?
  - https://airtable.com/universe, looks like a database frontend
8. "Toy Apps"
  - https://www.costarastrology.com/, AI astrology?
9. Widget Marketplace (maybe just simplifying widgets?)
  - https://go.widgetic.com/


(1) can either be a technical task, like managing content for static site, or a form-generating and checking task, like generating citizenship, bankruptcy, or tax paperwork.
(2) seems to involve applying some specific domain knowledge, in this case inventory management to make fewer steps for a business customer.

#### What programs would make the world a better place?

At the risk of sounding too idealistic, what are some problems (currently solvable by software) that, if solved would make my life better?

1. More tightly integrating various devices. Phone, laptop, tablet, desktop, etc. are all their own isolated environment. I still email files between my own computers (DropBox), and email short blurbs (shared clipboard?)
2. Streamlining airline miles? Currently, the system involves a lot of calculations and scheming on my part. Is there a way to have a single interface? (https://flightfox.com/)

Of all of the startups listed above, FlightFox seems the most wayward. Their business model revolves around using a lot of human interaction (single representative) to try and find the best deals. That representative may have access to software tools, but exposing that software to the web would seem better. Are there corporate limitations?

From the categories of startups, (1) seems the most likely to be able to be unioned.

None of these startups seem particularly conducive to an easy clone (my original task).

There's an API for mugshots, that's novel. https://rapidapi.com/jailbase/api/jailbase/details

#### Travel APIs
1. Hipmunk, https://rapidapi.com/apidojo/api/hipmunk1
2. Priceline (requires an 'established site'), https://pricelinepartnernetwork.com/api
3. Expedia, https://expediaconnectivity.com/developer
4. SkyScanner, https://blog.rapidapi.com/skyscanner-flight-api-incorporate-travel-data-into-your-app/ or https://partners.skyscanner.net/affiliates/travel-apis
5. Allmyles, https://allmyles.com/
6. Trawex, https://www.trawex.com/flight-suppliers.php
7. Matrix, https://matrix.itasoftware.com/


#### Travel ideas
Maybe a group discount travel site?
Where should I go? A site that cross references deals and season to recommend trips?
