---
title: Day Three
date: "2019-05-25T14:02:00.000Z"
---

Yesterday, I went off the rails a bit. I started with some defined goals and let the
search take its course.


# Lessons Learned from Yesterday

HN Who's Hiring seems to give an odd subset of startups. Looking at previous
YC batches seems to give a better cross section. Crunchbase isn't too useful
without the paid tier. LinkedIn is hard to use for anything other than individual
discovery.


# Goals for Today

-   [ ] Explore travel APIs and develop some opinions re: functionality
-   [ ] Start *Traction* (50 pages)
-   [ ] Set dates for move to Bali
-   [ ] Repost roommate search on CL and HUOCH

I am trying org mode compiled to Markdown instead of typing directly in Markdown for
this entry, we'll see how effective that ends up being.


# TODO Travel APIs from Yesterday's post

-   [ ] Hipmunk, <https://rapidapi.com/apidojo/api/hipmunk1>
-   [ ] Priceline (requires an 'established site'), <https://pricelinepartnernetwork.com/api>
-   [ ] Expedia, <https://expediaconnectivity.com/developer>
-   [ ] SkyScanner, <https://blog.rapidapi.com/skyscanner-flight-api-incorporate-travel-data-into-your-app/> or <https://partners.skyscanner.net/affiliates/travel-apis>
-   [ ] Allmyles, <https://allmyles.com/>
-   [ ] Trawex, <https://www.trawex.com/flight-suppliers.php>
-   [ ] Matrix, <https://matrix.itasoftware.com/>
